       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-GRADE1.
       AUTHOR. MONGKOL.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           05 STU-ID PIC X(8).
           05 MIDTERM-SCORE PIC 9(2)V9(2).
           05 FINAL-SCORE PIC 9(2)V9(2).
           05 PROJECT-SCORE PIC 9(2)V9(2).
       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE
           MOVE "62160296" TO STU-ID 
           MOVE "35.05" TO MIDTERM-SCORE 
           MOVE "24.25" TO FINAL-SCORE 
           MOVE "11.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160282" TO STU-ID 
           MOVE "34.05" TO MIDTERM-SCORE 
           MOVE "28.25" TO FINAL-SCORE 
           MOVE "14.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160289" TO STU-ID 
           MOVE "47.05" TO MIDTERM-SCORE 
           MOVE "30.25" TO FINAL-SCORE 
           MOVE "15.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160060" TO STU-ID 
           MOVE "34.05" TO MIDTERM-SCORE 
           MOVE "22.25" TO FINAL-SCORE 
           MOVE "12.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160068" TO STU-ID 
           MOVE "37.05" TO MIDTERM-SCORE 
           MOVE "25.25" TO FINAL-SCORE 
           MOVE "17.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 
           CLOSE SCORE-FILE 
           GOBACK
           .
